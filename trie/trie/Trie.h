#ifndef TRIE_H

/*
author : akdal, or aqjune, or juneyoung lee
last revision : 2014-04-19
*/
#define TRIE_H

#include<vector>
#include<assert.h>
#include<algorithm>
#include<iostream>

template<class T>
class Trie{
public:
	//static int NODE_ALLOCCNT;
	//static int NODE_DEALLOCCNT;
protected:
	class Node{
	private:
		std::vector<char> *c_edges;
		std::vector<Node*> *c_nodes;
		
		bool _hasD(int idx) const
		{ return optionMask & (1 << idx); }
		inline void _setD(int idx, bool d)
		{ if(!d) optionMask &= (0xFF - (1 << idx));
		else optionMask |= (1 << idx); }


	public:
		inline void hasValue(bool d){ _setD(0, d); }
		inline void isLeaf(bool d){ _setD(1, d); }
		inline void isConstant(bool d){ _setD(2, d); }
	public:
		inline bool isConstant() const{ return _hasD(2); }
		inline bool isLeaf() const{ return _hasD(1); }
		inline bool hasValue() const{ return _hasD(0); }

		T value;
		char optionMask;
	
		Node(T &value){
			this->optionMask = 0;

			this->hasValue(true);
			this->value = value;
			this->isLeaf(true);
			this->c_edges = NULL;
			this->c_nodes = NULL;
			this->isConstant(false);
			//Trie<T>::NODE_ALLOCCNT++;
		}
		Node(){
			this->optionMask = 0;

			this->hasValue(false);
			this->isLeaf(true);
			this->c_edges = NULL;
			this->c_nodes = NULL;
			this->isConstant(false);
			//Trie<T>::NODE_ALLOCCNT++;
		}
		Node(const Node &othernode){
			this->optionMask = othernode.optionMask;
			this->value = othernode.value;
			this->c_edges = NULL;
			this->c_nodes = NULL;
			if(othernode.c_edges != NULL){
				assert(othernode.c_nodes != NULL);
				assert(othernode.c_edges->size() == othernode.c_nodes->size());

				this->c_edges = new std::vector<char>();
				this->c_nodes = new std::vector<Node *>();
				for(int i = 0; i < othernode.c_edges->size(); i++){
					this->c_edges->push_back(othernode.c_edges->at(i));
					this->c_nodes->push_back(new Node(*(othernode.c_nodes->at(i))));
				}
			}
			//Trie<T>::NODE_ALLOCCNT++;
		}
		~Node(){
			int i;
			if(!isLeaf()){
				for(i = 0; i < this->c_nodes->size(); i++)
					delete (this->c_nodes->at(i));
				delete c_edges;
				delete c_nodes;
			}
			//Trie<T>::NODE_DEALLOCCNT++;
		}
		void makeConstant(){
			this->isConstant(true);
			if(isLeaf())
				return;
			int i, j;

			for(i = 0; i < this->size(); i++){
				char ci = this->c_edges->at(i);

				for(j = i + 1; j < this->size(); j++){
					char cj = this->c_edges->at(j);
					
					if(ci > cj){
						swap(this->c_nodes->at(i), this->c_nodes->at(j));
						swap(this->c_edges->at(i), this->c_edges->at(j));
						ci = cj;
					}
				}
			}
			for(i = 0; i < this->c_nodes->size(); i++)
				this->c_nodes->at(i)->makeConstant();
		}
		Node *getNode(char c) const {
			if(this->isLeaf())
				return NULL;

			if(this->isConstant()){
				int pos;
				int sz = this->size();
				if(sz == 0) return NULL;

				int st= 0, ed = sz - 1;
				while(true){
					if(st == ed){
						char cc = this->c_edges->at(st);
						if(cc == c)
							return this->c_nodes->at(st);
						else
							return NULL;
					}else if(st == ed - 1){
						char cc = this->c_edges->at(st);
						if(cc == c)
							return this->c_nodes->at(st);
						else{
							st = ed;
						}
					}else{
						int piv = (st + ed) / 2;
						char cc = this->c_edges->at(piv);

						if(cc == c)
							return this->c_nodes->at(piv);
						else if(cc < c){
							st = piv + 1;
						}else{
							ed= piv - 1;
						}
					}
				}
				return NULL;
			}else{
				int ll = this->size();
				for(int i = 0;i < ll; i++){
					if(this->c_edges->at(i) == c)
						return this->c_nodes->at(i);
				}
				return NULL;
			}
		}
		void deleteNode(char c){
			assert(!isConstant());

			if(this->isLeaf()){
				assert(false);
				return;
			}
			for(int i = 0; i < size(); i++){
				char cc = this->c_edges->at(i);
				if(cc == c){
					delete this->c_nodes->at(i);
					this->c_nodes->erase(this->c_nodes->begin() + i);
					this->c_edges->erase(this->c_edges->begin() + i);
					break;
				}
			}

			if(this->size() == 0){
				this->isLeaf(true);

				delete this->c_edges;
				this->c_edges = NULL;

				delete this->c_nodes;
				this->c_nodes = NULL;
			}
		}
		Node *childAt(int i) const
		{ return this->c_nodes->at(i); }
		char edgeAt(int i) const
		{ return this->c_edges->at(i); }
		int size() const
		{ assert(this->c_nodes->size() == this->c_edges->size()); return this->c_nodes->size(); }
		Node *addNode(char c){
			assert(!isConstant());
			if(this->isLeaf()){
				this->isLeaf(false);
				this->c_edges = new std::vector<char>();
				this->c_nodes = new std::vector<Node *>();
			}
			Node *n = new Node();
			this->c_edges->push_back(c);
			this->c_nodes->push_back(n);
			
			return n;
		}
		void print(std::ostream &ostr, int space) const{
			int i;
			if(this->hasValue()){
				for(i = 0; i < space; i++)
					ostr << (" ");
				ostr << "VALUE='" << this->value << "'" << endl;
			}
			if(!this->isLeaf())
				for(i = 0; i < this->size(); i++){
					int j;
					for(j = 0; j < space; j++)
						ostr << (" ");
					ostr << "edge '" << this->c_edges->at(i) << "'" << endl;
					this->c_nodes->at(i)->print(ostr, space + 1);
				}
		}
	};
	
public:
	class iterator{
	private:
		char *trace;
		int *stack;
		(Node *)* nodeStack;
		//int stackSize;
		int nodeStackSize;
		int maxStackSize;
		bool end;
		Node *root;

	protected:

		void _nextItr(){
			if(end)
				return;
			if(this->nodeStackSize == 0){
				this->nodeStackSize = 1;
				this->nodeStack[0] = root;
				this->trace[0] = 0;
				return;
			}
			if(!this->nodeStack[this->nodeStackSize - 1]->isLeaf()){
				this->nodeStack[this->nodeStackSize] = this->nodeStack[this->nodeStackSize - 1]->childAt(0);
				this->trace[this->nodeStackSize - 1] = this->nodeStack[this->nodeStackSize - 1]->edgeAt(0);
				this->trace[this->nodeStackSize] = 0;
				this->stack[this->nodeStackSize -1] = 0;
				this->nodeStackSize++;
			}else{
				while(true){
					this->nodeStackSize--;
					if(this->nodeStackSize == 0){
						end = true;
						break;
					}
					this->stack[this->nodeStackSize - 1]++;
					int idx = this->stack[this->nodeStackSize - 1];
					if(idx < this->nodeStack[this->nodeStackSize - 1]->size()){
						this->nodeStack[this->nodeStackSize] = this->nodeStack[this->nodeStackSize - 1]
								->childAt(idx);
						this->trace[this->nodeStackSize - 1] = this->nodeStack[this->nodeStackSize - 1]->edgeAt(idx);
						this->trace[this->nodeStackSize] = 0;
						this->nodeStackSize++;
						break;
					}
				}
			}
		}
	public:
		iterator(int maxStackSize, Node *root){
			this->maxStackSize = maxStackSize;
			this->nodeStackSize = 0;
			//this->stackSize = 0;

			this->trace = new char[this->maxStackSize + 2];
			this->stack = new int[this->maxStackSize + 1];
			this->nodeStack = new Trie<T>::Node *[this->maxStackSize + 2];
			this->end = false;
			this->root = root;
		}
		iterator(const iterator &other){
			this->maxStackSize = other.maxStackSize;
			this->nodeStackSize = other.nodeStackSize;
			this->root = other.root;
			this->end = other.end;
			
			this->trace = new char[this->maxStackSize + 2];
			memcpy(this->trace, other.trace, this->maxStackSize + 2);;

			this->stack = new int[this->maxStackSize + 1];
			memcpy(this->stack, other.stack, (this->maxStackSize + 1) * sizeof(int));

			this->nodeStack = new Node*[this->maxStackSize + 2];
			memcpy(this->nodeStack, other.nodeStack, (this->maxStackSize + 2) * sizeof(Node *));
		}
		
		iterator& operator=(const iterator &other){
			if(this->nodeStack)
				delete this->nodeStack;
			if(this->stack)
				delete this->stack;
			if(this->trace)
				delete this->trace;

			this->maxStackSize = other.maxStackSize;
			this->nodeStackSize = other.nodeStackSize;
			this->root = other.root;
			this->end = other.end;
			
			this->trace = new char[this->maxStackSize + 2];
			memcpy(this->trace, other.trace, this->maxStackSize + 2);;

			this->stack = new int[this->maxStackSize + 1];
			memcpy(this->stack, other.stack, (this->maxStackSize + 1) * sizeof(int));

			this->nodeStack = new Node*[this->maxStackSize + 2];
			memcpy(this->nodeStack, other.nodeStack, (this->maxStackSize + 2) * sizeof(Node *));

			return *this;
		}
		iterator(iterator &&other){
			this->trace = other.trace;
			this->stack = other.stack;
			this->nodeStack = other.nodeStack;
			other.trace = NULL;
			other.stack = NULL;
			other.nodeStack = NULL;

			this->maxStackSize = other.maxStackSize;
			this->nodeStackSize = other.nodeStackSize;
			this->root = other.root;
			this->end = other.end;
		}
		~iterator(){
			if(trace)
				delete trace;
			if(stack)
				delete stack;
			if(nodeStack)
				delete nodeStack;
		}

		bool isEnd() const
		{ return this->end; }
		T *next(){
			if(end)
				return NULL;

			do{
				_nextItr();
			}while(!end && !this->nodeStack[this->nodeStackSize - 1]->hasValue());
			
			T * answer;
			if(this->nodeStackSize == 0)
				answer = NULL;
			else
				answer=   &this->nodeStack[this->nodeStackSize - 1]->value;
			
			return answer;
		}
		const char *getCurrentTrace() const
		{ return this->trace; }
	};


private:
	Node *rootNode;
	int maxDepth;
	long long int elemSize;
	bool doConstant;
public:
	Trie(){
		this->rootNode = new Node();
		this->maxDepth = 1;
		this->doConstant = false;
		this->elemSize= 0;
	}
	Trie(const Trie &orgtrie){
		this->rootNode = new Node(*(orgtrie.rootNode));
		this->maxDepth = orgtrie.maxDepth;
		this->elemSize = orgtrie.elemSize;
		this->doConstant = false;
	}
	void makeConstant()
	{ this->doConstant = true; this->rootNode->makeConstant(); }
	void put(const char *letters, const T& value);
	void put(const std::string &str, const T& value);
	void erase(const std::string &str){
		assert(!doConstant);
		this->_erase(str.c_str(), this->rootNode);
		this->elemSize--;
	}
	T *get(const char *letters);
	T *get(const std::string &str);
	typename Trie<T>::iterator itr();
	void clear();
	long long int size() const
	{ return this->elemSize; }

	void print(std::ostream &st)
	{this->rootNode->print(st, 0); }

protected:
	// returns : new node?
	bool _put(const char *letters, Node *focus, const T& t);
	bool _erase(const char *chrs, Node *node){
		if(*chrs == 0){
			node->hasValue(false);
			return node->isLeaf();
		}
		Trie<T>::Node *n = node->getNode(*chrs);
		assert(n != NULL);
		if(this->_erase(chrs + 1, n) == true){
			node->deleteNode(*chrs);
		}
		if(node->isLeaf() && !node->hasValue())
			return true;
		return false;
	}
	T *_get(const char *letters, Node *focus);
	T *_get(const std::string &str, int idx, int len, Node *focus);
};


template<class T>
bool Trie<T>::_put(const char *letters, Node *focus, const T& t){
	if(letters[0] == 0){
		bool returnv = focus->hasValue();
		focus->hasValue(true);
		focus->value = t;
		return !returnv;
	}else{
		Trie<T>::Node *n = focus->getNode(letters[0]);
		if(n == NULL)
			return this->_put(letters + 1, focus->addNode(letters[0]), t);
		else
			return this->_put(letters + 1, n, t);
	}
}
template<class T>
void Trie<T>::put(const char *letters, const T &t)
{ 
	assert(!this->doConstant);
	int len = strlen(letters) + 1;
	if(len > this->maxDepth)
		this->maxDepth = len;
	if(this->_put(letters, this->rootNode, t))
		this->elemSize++;
}
template<class T>
void Trie<T>::put(const std::string &str, const T& value){
	assert(!this->doConstant);
	this->put(str.c_str(), value);
}



template<class T>
T* Trie<T>::_get(const char *letters, Node *node){
	if(letters[0] == 0)
		return node->hasValue() ? &(node->value) : NULL;
	Trie<T>::Node *n = node->getNode(letters[0]);
	if(n == NULL)
		return NULL;
	return this->_get(letters + 1, n);
}
template<class T>
T* Trie<T>::_get(const std::string &str, int idx, int len, Node *node){
	if(idx == len)
		return node->hasValue() ? &(node->value) : NULL;
	Trie<T>::Node *n = node->getNode(str.at(idx));
	if(n == NULL)
		return NULL;
	return this->_get(str, idx + 1, len, n);
}


template<class T>
T *Trie<T>::get(const char *letters)
{ return this->_get(letters, this->rootNode); }
template<class T>
T *Trie<T>::get(const std::string &letters)
{ return this->_get(letters, 0, letters.length(), this->rootNode); }
template<class T>
typename Trie<T>::iterator Trie<T>::itr(){
	return Trie<T>::iterator(this->maxDepth, this->rootNode);
}

template<class T>
void Trie<T>::clear(){
	delete this->rootNode;
	this->doConstant = false;
	this->maxDepth = 0;
	this->rootNode = new Node();
	this->elemSize = 0;
}

#endif
