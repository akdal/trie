#include"Trie.h"
#include<assert.h>
#include<iostream>
#include<ctime>
#include<random>
#include<string>
using namespace std;


int main(){
	Trie<int> t;
	Trie<bool> tt;
	t.put("aaaaa", 5);
	t.put("a", 1);
	t.put("ab", 2);
	t.put("bc", 2);
	t.put("b", 1);
	t.put("테스트", 4); // Korean also okay
	t.put("테스블", 5);

	int *p_aaa = t.get("aaa");
	int *p_a = t.get("a");
	int *p_dd = t.get("dd");
	int *p_bc = t.get("bc");
	int *p_ab = t.get("ab");
	int *p_b = t.get("b");

	t.print(cout);
	Trie<int>::iterator itr = t.itr();
	while(!itr.isEnd()){
		int *val = itr.next();
		if(val == NULL)
			break;
		cout << itr.getCurrentTrace() << " : " << *val << endl;
	}
	t.erase("bc");
	t.print(cout);
	cout<<"--"<<endl;
	t.erase("a");
	t.erase("aaaaa");
	t.print(cout);
	cout<<"--"<<endl;

	return 0;
}