#include"Trie.h"
#include<assert.h>
#include<iostream>
#include<ctime>
#include<random>
#include<string>
using namespace std;

vector<string> testStrings;
Trie<int> trie;

void genRandStrings(){
	std::mt19937 generator(time(0));
	std::uniform_int_distribution<int> dist(1, 15);
	std::uniform_int_distribution<int> dist2('a', 'z');
	
	for(int i = 0; i < 4000; i++){
		int len = dist(generator);
		string s;
		while(len--){
			s.push_back(dist2(generator));
		}
		if(find(testStrings.begin(), testStrings.end(), s) == testStrings.end())
			testStrings.push_back(s);
		else
			i--;
	}
}
int main(){
	genRandStrings();
	for(int i = 0;i < testStrings.size(); i++)
		trie.put(testStrings[i], i);

	Trie<int>::iterator itr = trie.itr();
	while(true){
		int *val = itr.next();
		if(val == NULL){
			assert(itr.isEnd()); // must be!
			break;
		}
		const char *trace = itr.getCurrentTrace();
		vector<string>::iterator itrpos = find(testStrings.begin(),testStrings.end(), string(trace));
		int posInVec=itrpos - testStrings.begin();

		assert(itrpos != testStrings.end());
		if(*val != posInVec){
			cout << "Problem! *val : " << *val << " , index in posInVec : " << posInVec << " , actual str : " << testStrings[posInVec] << endl;
			assert((*val) == posInVec);
		}
	}
	cout << "iteration works well!good :) End" << endl;
	int tt;
	cin >> tt;
	return 0;
}