#include"Trie.h"
#include<assert.h>
#include<iostream>
#include<ctime>
#include<random>
#include<string>
using namespace std;

vector<string> testStrings;

void genRandStrings(){
	std::mt19937 generator(time(0));
	std::uniform_int_distribution<int> dist(1, 15);
	std::uniform_int_distribution<int> dist2('a', 'z');

	for(int i = 0; i < 10000; i++){
		int len = dist(generator);
		string s;
		while(len--){
			s.push_back(dist2(generator));
		}
		if(find(testStrings.begin(), testStrings.end(), s) == testStrings.end())
			testStrings.push_back(s);
		else
			i--;
	}
}

int main(){
	genRandStrings();
	Trie<bool> tt;
	for(int i = 0;i < testStrings.size(); i++){
		tt.put(testStrings[i], true);
	}
	tt.makeConstant();
	for(int i = 0;i < testStrings.size(); i++){
		if(tt.get(testStrings[i]) == NULL){
			cout << "COULDNT FIND!! : " << testStrings[i] << endl;
			tt.print(cout);
			assert(false);
		}
	}
	cout << "makeConstant() test end! cool!\n";
	int ttt;
	cin >> ttt;
	return 0;
}